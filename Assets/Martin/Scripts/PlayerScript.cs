using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    public float horizontalMovie;
    public float verticalMovie;
    public CharacterController player;
    public Text puntaje;

    public float playerSpeed;
    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontalMovie = Input.GetAxis("Horizontal");
        verticalMovie = Input.GetAxis("Vertical");
    }
}
