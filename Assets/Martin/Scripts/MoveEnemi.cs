using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveEnemi : MonoBehaviour
{
    public int rutina;
    public float cronometro;
    public Animator ani;
    public Quaternion angulo;
    public float grado;
    public float walk = 0f;
    public float run = 0f;
    public bool atacando;

    public GameObject target;


    void Start()
    {
        ani = GetComponent<Animator>();
        target = GameObject.Find("Personaje01");
    }

    // Update is called once per frame
    void Update()
    {
        EnemiBehavior();
    }

    public void EnemiBehavior()
    {
        if(Vector3.Distance(transform.position, target.transform.position) > 30)
        {
            ani.SetBool("Run", false);
            cronometro += 1 * Time.deltaTime;
            if(cronometro > +4)
            {
                rutina = Random.Range(0, 2);
                cronometro = 0;
            }

            switch(rutina)
            {
                case 0:
                    ani.SetBool("Walk", false);
                    break;
                case 1:
                    grado = Random.Range(0, 360);
                    angulo = Quaternion.Euler(0, grado, 0);
                    rutina++;
                    break;
                case 2:
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, angulo, 0.5f);
                    transform.Translate(Vector3.forward * walk * Time.deltaTime);
                    ani.SetBool("Walk", true);
                    break;
            }
        }else
        {
            if (Vector3.Distance(target.transform.position, transform.position) > 2 && !atacando)
            {
                var lookPos = target.transform.position - transform.position;
                lookPos.y = 0;
                var rotation = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 3);
                ani.SetBool("Walk", false);

                ani.SetBool("Run", true);
                transform.Translate(Vector3.forward * run * Time.deltaTime);

                ani.SetBool("Attack", false);
                   
            }else
            {
                ani.SetBool("Walk", false);
                ani.SetBool("Run", false);

                ani.SetBool("Attack", true);
                atacando = true;
            }
        }
    }

    public void Final_Ani()
    {
        ani.SetBool("Attack", false);
        atacando = false;
    }
}
