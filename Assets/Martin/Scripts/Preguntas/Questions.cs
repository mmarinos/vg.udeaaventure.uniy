using System.Collections.Generic;
using UnityEngine;

public class Questions : MonoBehaviour
{
    public string text;
    public List<Option> options = new List<Option>();
}
