using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using Newtonsoft.Json;
using System;
//using Newtonsoft.Json.Linq;

public class QuizDB : MonoBehaviour
{
    [SerializeField] private List<Questions> m_questionsList = new List<Questions>();

    private List<Questions> m_backup = new List<Questions>();
    public string ganoNivel = "Gano";
    public string perdioNivel = "Gano";
    private Inventario inventario;
    private double validaPorcentaje;
    private double porcentaje = 0.70;

    //public int totalpuntajequiz = 0;
    public double totalquestios = 0;

    private void Awake()
    {
        routineDB();
        totalquestios = m_questionsList.Count;
        //m_backup = m_questionsList.ToList();
        inventario = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventario>();
    }

    public Questions GetRandom(bool remove = true)
    {
        
        if (m_questionsList.Count == 0)
        {
            validaPorcentaje = Math.Round(totalquestios * porcentaje);
            if(validaPorcentaje >= inventario.totalpuntajequiz)
            {
                SceneManager.LoadScene(ganoNivel);
            }

            if (validaPorcentaje < inventario.totalpuntajequiz)
            {
                SceneManager.LoadScene(perdioNivel);
            }
        }

        int index = UnityEngine.Random.Range(0, m_questionsList.Count);

        if (!remove)
            return m_questionsList[index];

        Questions q = m_questionsList[index];
        m_questionsList.RemoveAt(index);

        return q;
    }

    private void RestoreBackup()
    {
        m_questionsList = m_backup.ToList();
    }

    private void routineDB()
    {
        readDB();
        Questions questions1 = new Questions();

        questions1.text = "Hay un debate intercolegiado sobre si se debe permitir o no el uso de los piercing a los estudiantes.Para participar con un art�culo en favor del uso de los piercing, tu escrito podr�a tener como t�tulo: ";
        questions1.options.Add(new Option{ text = "A. Los jovenes y sus padres", correct = false });
        questions1.options.Add(new Option{ text = "B. Mas libertad en los colegios", correct = true });
        questions1.options.Add(new Option{ text = "C. Menos tatuajes y mas salud", correct = false });
        questions1.options.Add(new Option{ text = "D. Los profesores y sus derechos", correct = false });

        m_questionsList.Add(questions1);


    }

    private void readDB()
    {


        string line;
        string path;

        path = System.Environment.CurrentDirectory;
        //line = Path.Combine(System.Environment.CurrentDirectory, "DBQuestions.json");
        //Debug.Log(path);
        try
        {
            //Pass the file path and file name to the StreamReader constructor
            //Debug.Log(path + "\\Assets\\Martin\\DB\\DBQuestions.json rutaaa");
            StreamReader sr = new StreamReader(path + "\\Assets\\Martin\\DB\\DBQuestions.txt");

            line = sr.ReadToEnd();
            //JObject json = JObject.Parse(line);
            //Debug.Log(line);
            List<Questions> questions2 = JsonConvert.DeserializeObject<List<Questions>>(line.ToString());

            m_questionsList = questions2.ToList();

            //Debug.Log(questions2);
            
        }
        catch (System.Exception e)
        {
            print("Exception: " + e.Message);
        }
    }
}
