using UnityEngine;

[RequireComponent(typeof(InputController))]
public class InputController : MonoBehaviour
{
    //Entradas de movimiento
    public Vector3 MoveInput()
    {
        float horizontalMovie = Input.GetAxis("Horizontal");
        float verticalMovie = Input.GetAxis("Vertical");

        return new Vector3(horizontalMovie, 0, verticalMovie);

    }

    //Entrada mouse para mover la camara
    public Vector3 MouseInput()
    {
        float x = Input.GetAxis("Mouse X");
        float y = Input.GetAxis("Mouse Y");

        return new Vector3(x, y, 0);
    }
}