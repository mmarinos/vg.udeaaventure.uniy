using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemigo : MonoBehaviour
{
    //==========variables movimiento============
    //public float velocidadMovimiento = 5.0f;
    //public float velocidadRotacion = 200.0f;
    private Animator anim;
    public float x, z;
    private float posx, posz;

    //==========================================
    public GameObject Target;
    public NavMeshAgent agent;
    public float velocidadEnemigo = 5f;
    public float distance;
   // public float velocidadRotacion = 180f;
    public float timeWalk = 10f;
    public Vector3 newPosicion;

    public VidaPersonaje vidaScript;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (Vector3.Distance(Target.transform.position, transform.position) < distance)
        {
            agent.SetDestination(Target.transform.position);
            agent.speed = velocidadEnemigo;
        } else
        {
            timeWalk -= Time.deltaTime;
            //Debug.Log(tiempoCaminar);
            if(timeWalk <= 0)
            {
                moveEnemigo();
                timeWalk = Random.Range(5f, 10f);
            }else
            {
                validatePosition();
            }
        }
        
        if (Vector3.Distance(Target.transform.position, transform.position) <= 2)
        {
            vidaScript.vidaActual -= 1;
        }
    }

    void validatePosition()
    {
        posx = transform.position.x;
        posz = transform.position.z;
        Debug.Log(posx + "-" + posz + "--" + newPosicion.x + "-" + newPosicion.z);
        if (newPosicion.x == posx && newPosicion.z == posz)
        {
            anim.SetBool("Reposo", true);
        }

    }

    void moveEnemigo() {
        x = Random.onUnitSphere.x * 15;
        z = Random.onUnitSphere.z * 15;
        newPosicion = transform.position + new Vector3(x, 0f, z);
        anim.SetFloat("VelX", x);
        anim.SetFloat("VelY", z);
        agent.SetDestination(newPosicion);
        agent.speed = velocidadEnemigo;
        anim.SetBool("Reposo", false);
    }

    //void caminarEnemigo()
    //{
    //    transform.Rotate(0, x * Time.deltaTime * velocidadRotacion, 0);
    //    transform.Translate(0, 0, y * Time.deltaTime * velocidadMovimiento);
    //}
}
