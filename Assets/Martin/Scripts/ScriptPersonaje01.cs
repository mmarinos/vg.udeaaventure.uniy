using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScriptPersonaje01 : MonoBehaviour
{
    public float velocidadMovimiento = 5.0f;
    public float velocidadRotacion = 200.0f;
    private Animator anim;
    private GameManager manager;
    public float x, y;

    public Text puntaje;
    public GameObject m_quizui;
    public GameObject controlSalir;

    public string pPierdeNivel;


    public Rigidbody rb;
    public float fuerzaDeSalto = 8f;
    public bool puedoSaltar;
    public VidaPersonaje vidaScript;
    void Start()
    {
        puedoSaltar = false;
        anim = GetComponent<Animator>();
        manager = GetComponent<GameManager>();
    }

    void FixedUpdate()
    {
        transform.Rotate(0, x * Time.deltaTime * velocidadRotacion, 0);
        transform.Translate(0, 0, y * Time.deltaTime * velocidadMovimiento);
    }

    void Update()
    {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        anim.SetFloat("VelX", x);
        anim.SetFloat("VelY", y);

        if (puedoSaltar)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                anim.SetBool("salte", true);
                rb.AddForce(new Vector3(0, fuerzaDeSalto, 0), ForceMode.Impulse);
            }
            anim.SetBool("tocoSuelo", true);
        }else
        {
            EstoyCayendo();
        }



        if (Input.GetKeyDown(KeyCode.Escape) && m_quizui.activeSelf)
        {
            
            m_quizui.SetActive(false);
        } else if(Input.GetKeyDown(KeyCode.Escape))
        {
            controlSalir.SetActive(true);
        }

        
    }

    public void EstoyCayendo()
    {
        anim.SetBool("tocoSuelo", false);
        anim.SetBool("salte", false);
    }

    private void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("arma"))
        {
            vidaScript.vidaActual -= 1;
        }

        if (coll.CompareTag("tablequiz"))
        {
            m_quizui.SetActive(true);
            manager.NextQuestion();
        }

        if (vidaScript.vidaActual <= 0)
        {
            SceneManager.LoadScene(pPierdeNivel);
        }
    }

}
