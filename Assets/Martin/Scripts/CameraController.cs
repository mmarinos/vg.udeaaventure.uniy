using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float mouseX = 0f;
    public float mouseY = 0f;
    private float yRotation;
    private Vector3 playerInput;
    InputController inputController = null;
    public Transform cameraAnchor = null;
    Camera camera;
    public float rotationCabeceo;

    // Start is called before the first frame update
    void Start()
    {
        inputController = GetComponent<InputController>();
        camera = GetComponentInChildren<Camera>();
        rotationCabeceo = camera.transform.rotation.x;
    }

    // Update is called once per frame
    void Update()
    {
        MouseCamera();
    }

    void MouseCamera()
    {
        playerInput = inputController.MouseInput();
        transform.Rotate(Vector3.up * playerInput.x * mouseX * Time.deltaTime);

        yRotation = playerInput.y * mouseY * Time.deltaTime;
        rotationCabeceo -= playerInput.y;
        rotationCabeceo = Mathf.Clamp(rotationCabeceo, -45, 45);
        camera.transform.localRotation = Quaternion.Euler(rotationCabeceo, 0, 0);

        //Vector3 angle = cameraAnchor.eulerAngles;
        //angle.x += playerInput.y * mouseY * Time.deltaTime;
        //cameraAnchor.eulerAngles = angle;
    }
}
