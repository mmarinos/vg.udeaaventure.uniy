using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Salir : MonoBehaviour
{
    public GameObject controlSalir;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void exit()
    {
        Application.Quit();
    }

    public void cancelar()
    {
        controlSalir.SetActive(false);
    }
}
