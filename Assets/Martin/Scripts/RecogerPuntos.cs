using UnityEngine;
using UnityEngine.UI;

public class RecogerPuntos : MonoBehaviour
{
    public Inventario inventario;
    public Text puntaje;
    // Start is called before the first frame update
    void Start()
    {
        inventario = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventario>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            inventario.TotalPuntos++;
            puntaje.text = inventario.TotalPuntos.ToString();
            Destroy(gameObject);
        }
        
    }
}
