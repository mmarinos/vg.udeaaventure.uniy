using UnityEngine;

public class scripPlayer : MonoBehaviour
{
    //public float horizontalMovie;
    //public float verticalMovie;
    public CharacterController player;

    InputController inputController = null;

    public float playerSpeed;
    private Vector3 moviePlayer;
    //private Vector3 playerInput;
    public float gravity = 9.8f;
    public float fallVelocity;
    public float jumpForce;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<CharacterController>();
        inputController = GetComponent<InputController>();
    }

    // Update is called once per frame
    void Update()
    {
        //horizontalMovie = Input.GetAxis("Horizontal");
        //verticalMovie = Input.GetAxis("Vertical");

        //playerInput = new Vector3(horizontalMovie,0,verticalMovie);
        //playerInput = Vector3.ClampMagnitude(playerInput, 1);

        //moviePlayer = playerInput * playerSpeed;
        Move();
        SetGravity();
        PlayerSkills();

        player.Move(moviePlayer * Time.deltaTime);
    }


    void Move()
    {
        Vector3 playerInput = inputController.MoveInput();
        playerInput = Vector3.ClampMagnitude(playerInput, 1);
        moviePlayer = playerInput * playerSpeed;
    }


    public void PlayerSkills()
    {
        if (player.isGrounded && Input.GetButtonDown("Jump"))
        {
            fallVelocity = jumpForce;
            moviePlayer.y = fallVelocity;
        }

    }

    //funcion de gravedad
    void SetGravity()
    {
        //en el aire
        if (player.isGrounded)
        {
            fallVelocity = -gravity * Time.deltaTime;
            moviePlayer.y = fallVelocity;
        }
        //en el suelo
        else
        {
            fallVelocity -= gravity * Time.deltaTime;
            moviePlayer.y = fallVelocity;
        }
    }
}
